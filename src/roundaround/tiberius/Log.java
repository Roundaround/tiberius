package roundaround.tiberius;

public class Log {
	
	enum Direction {
		IN,
		OUT
	}
	
	public static void info(String str) {
		System.out.println(str);
	}
	
	public static void err(String str) {
		System.err.println(str);
	}
	
	public static void logIn(String nick, String str) {
		logRaw(Direction.IN, nick, str);
	}
	
	public static void logOut(String nick, String str) {
		logRaw(Direction.OUT, nick, str);
	}
	
	public static void logRaw(Direction dir, String nick, String str) {
		if (dir == Direction.IN)
			str = nick + " << " + str;
		else if (dir == Direction.OUT)
			str = nick + " >> " + str;
		
		System.out.println(str);
	}
	
	public static void logIn(String str) {
		logRaw(Direction.IN, str);
	}
	
	public static void logOut(String str) {
		logRaw(Direction.OUT, str);
	}
	
	public static void logRaw(Direction dir, String str) {
		if (dir == Direction.IN)
			str = "<< " + str;
		else if (dir == Direction.OUT)
			str = ">> " + str;
		
		System.out.println(str);
	}
}
