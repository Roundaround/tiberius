package roundaround.tiberius;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class IOManager {
	
    private BufferedReader reader;
    private BufferedWriter writer;
    private Core bot;
    
    public IOManager(Core bot, InputStream input, OutputStream output) {
    	this.bot = bot;
        reader = new BufferedReader(new InputStreamReader(input));
        writer = new BufferedWriter(new OutputStreamWriter(output));
    }
    
    public void writeRaw(String text) throws IOException {
    	writer.write(text + "\r\n");
    	writer.flush();
    	Log.logOut(bot.getCurrNick(), text);
    }
    
    public String readRaw() throws IOException {
    	String read = reader.readLine();
    	Log.logIn(bot.getCurrNick(), read);
    	return read;
    }
    
    public Message read() throws IOException {
    	String read = reader.readLine();
    	Log.logIn(bot.getCurrNick(), read);
    	return Message.parse(read);
    }
    
    public void message(String destination, String message) throws IOException {
        writeRaw(Irc.PRIVMSG + " " + destination + " :" + message);
    }
    
    public void notice(String nick, String message) throws IOException {
    	writeRaw(Irc.NOTICE + " " + nick + " :" + message);
    }
    
    public void who(String nick) throws IOException {
    	writeRaw(Irc.WHO + " " + nick + " %uhnar");
    }
    
    public void whois(String nick) throws IOException {
    	writeRaw(Irc.WHOIS + " " + nick);
    }
}
