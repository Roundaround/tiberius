package roundaround.tiberius;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class Cache {
	private class CachedWrapper {
		private long cachedDate;
		private HashMap<Object, Object> map;
		
		public CachedWrapper() {
			cachedDate = System.currentTimeMillis();
			map = new HashMap<Object, Object>();
		}

		public long getCachedDate() {
			return cachedDate;
		}

		public HashMap<Object, Object> getMap() {
			return map;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + (int) (cachedDate ^ (cachedDate >>> 32));
			result = prime * result + ((map == null) ? 0 : map.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CachedWrapper other = (CachedWrapper) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (cachedDate != other.cachedDate)
				return false;
			if (map == null) {
				if (other.map != null)
					return false;
			} else if (!map.equals(other.map))
				return false;
			return true;
		}

		private Cache getOuterType() {
			return Cache.this;
		}
	}
	
	private final Map<String, CachedWrapper> cache = new TreeMap<String, CachedWrapper>();

	public synchronized boolean containsKey(String type, Object key) {
		if (cache.containsKey(type))
			return cache.get(type).getMap().containsKey(key);
		return false;
	}

	public synchronized Object get(String type, Object key) {
		if (cache.containsKey(type))
			return cache.get(type).getMap().get(key);
		return null;
	}

	public synchronized Object put(String type, Object key, Object value) {
		CachedWrapper newItem;
		if (cache.containsKey(type)) {
			newItem = cache.get(type);
		} else {
			newItem = new CachedWrapper();
			cache.put(type, newItem);
		}
		return newItem.getMap().put(key, value);
	}
	
	public synchronized void remove(String type, Object key) {
		if (cache.containsKey(type))
			cache.get(type).getMap().remove(key);
	}
	
	public synchronized long getCachedDate(String type, Object key) {
		if (cache.containsKey(type))
			return cache.get(type).getCachedDate();
		return -1;
	}
}
