package roundaround.tiberius;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import roundaround.tiberius.Utils.Pair;
import roundaround.tiberius.Utils.User;
import roundaround.tiberius.config.Configuration;
import roundaround.tiberius.config.Property;
import roundaround.tiberius.db.DataObjects;
import roundaround.tiberius.evnt.EventEndOfNames;
import roundaround.tiberius.evnt.EventJoin;
import roundaround.tiberius.evnt.EventKick;
import roundaround.tiberius.evnt.EventManager;
import roundaround.tiberius.evnt.EventMessage;
import roundaround.tiberius.evnt.EventNames;
import roundaround.tiberius.evnt.EventNick;
import roundaround.tiberius.evnt.EventPart;
import roundaround.tiberius.evnt.EventPing;
import roundaround.tiberius.evnt.EventPrivMsg;
import roundaround.tiberius.evnt.EventQuit;
import roundaround.tiberius.evnt.EventSelfJoin;
import roundaround.tiberius.evnt.EventSelfKick;
import roundaround.tiberius.evnt.EventSelfPart;
import roundaround.tiberius.evnt.EventWho;
import roundaround.tiberius.evnt.EventWhoIs;
import roundaround.tiberius.exception.MissingConfigurationAttributeException;
import roundaround.tiberius.exception.MissingConfigurationFieldException;
import roundaround.tiberius.listener.ListenerCommand;
import roundaround.tiberius.listener.ListenerFactoid;
import roundaround.tiberius.listener.ListenerUser;

public class Core implements Runnable {
    public static final String configFile = "TiberiusConfig.xml";
    public static final String version = "Tiberius IRC Bot v0.20a - (C) 2014 Evan Steinkerchner";

    public static ArrayList<Core> bots = new ArrayList<Core>();
    
    private EventManager evnt = new EventManager();
    private Cache cache = new Cache();
    private Configuration config = null;
    private ServerConnection server = null;
    private DataObjects db = null;

    private String dbName;
    private String nick;
    private String nspass;
    private String nsident;
    private String altnick;
    private String currnick;
    private String ext;
    private String user;
    private String name;
    private String cmdprefix;
    private String fctdprefix;
    private long chanReclaimDuration;

    private boolean keepExecuting = true;
    private long starttime;
    private HashSet<String> admins = new HashSet<String>();
    private HashSet<User> trackedUsers = new HashSet<User>();
    private ConcurrentHashMap<String, String> channels = new ConcurrentHashMap<String, String>();
    private Set<String> currentChannels = Collections.newSetFromMap(new ConcurrentHashMap<String, Boolean>());
    private Set<Pair<String, String>> userChanMap = Collections.newSetFromMap(new ConcurrentHashMap<Pair<String, String>, Boolean>());

    private boolean useAlt = false;
    private short extCount = 0;

    // private long lastNickReclaim = System.currentTimeMillis();
    private long lastChanReclaim = System.currentTimeMillis();

    // TODO: Add connection checks to setters. Don't allow sets if connected. "Use 'change' instead."
    public long getStartTime() {
        return starttime;
    }

    public void setStartTime(long starttime) {
        this.starttime = starttime;
    }
    
    public String getDBName() {
        return dbName;
    }
    
    public void setDBName(String dbName) {
        this.dbName = dbName;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getNSPass() {
        return nspass;
    }

    public void setNSPass(String nspass) {
        this.nspass = nspass;
    }

    public String getNSIdent() {
        return nsident;
    }

    public void setNSIdent(String nsident) {
        this.nsident = nsident;
    }

    public String getAltNick() {
        return altnick;
    }

    public void setAltNick(String altnick) {
        this.altnick = altnick;
    }

    public String getCurrNick() {
        return currnick != null ? currnick : nick;
    }

    public void setCurrNick(String nick) {
        currnick = nick;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCmdPrefix() {
        return cmdprefix;
    }

    public void setCmdPrefix(String cmdprefix) {
        this.cmdprefix = cmdprefix;
    }

    public String getFctdPrefix() {
        return fctdprefix;
    }

    public void setFctdPrefix(String fctdprefix) {
        this.fctdprefix = fctdprefix;
    }
    
    public long getChanReclaimDuration() {
        return chanReclaimDuration;
    }
    
    public void setChanReclaimDuration(long chanReclaimDuration) {
        this.chanReclaimDuration = chanReclaimDuration;
    }

    public ServerConnection getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = new ServerConnection(this, server);
    }

    public void setServer(String server, int port) {
        this.server = new ServerConnection(this, server, port);
    }
    
    public DataObjects getDataObjects() {
        return this.db;
    }

    public HashSet<String> getAdmins() {
        return admins;
    }

    public void addAdmin(String admin) {
        admins.add(admin);
    }

    public void removeAdmin(String admin) {
        admins.remove(admin);
    }

    public IOManager io() {
        return getServer().getIOManager();
    }

    public EventManager evnt() {
        return evnt;
    }

    public synchronized boolean isCached(Object key) {
        return cache.containsKey(Thread.currentThread().getStackTrace()[2].getClassName(), key);
    }

    public synchronized Object getCache(Object key) {
        return cache.get(Thread.currentThread().getStackTrace()[2].getClassName(), key);
    }

    public synchronized Object putCache(Object key, Object value) {
        return cache.put(Thread.currentThread().getStackTrace()[2].getClassName(), key, value);
    }

    public synchronized void removeCache(Object key) {
        cache.remove(Thread.currentThread().getStackTrace()[2].getClassName(), key);
    }

    public synchronized long getCachedDate(Object key) {
        return cache.getCachedDate(Thread.currentThread().getStackTrace()[2].getClassName(), key);
    }

    public HashSet<User> getTrackedUsers() {
        return trackedUsers;
    }

    public User getUserByNick(String nick) {
        for (User user : trackedUsers) {
            if (user.getNick().equals(nick))
                return user;
        }
        return null;
    }

    public Set<Pair<String, String>> getUserChanMap() {
        return userChanMap;
    }

    public boolean areChanUsersMapped(String channel) {
        for (Pair<String, String> mapping : userChanMap) {
            if (mapping.getRight().equalsIgnoreCase(channel) && mapping.getLeft().equals("*"))
                return true;
        }
        return false;
    }
    
    public ConcurrentHashMap<String, String> getChannels() {
        return channels;
    }
    
    public void addChannel(String channel) {
        channels.put(channel.toLowerCase(), "");
    }
    
    public void addChannel(String channel, String key) {
        channels.put(channel.toLowerCase(), key == null ? "" : key);
    }
    
    public void removeChannel(String channel) {
        channels.remove(channel.toLowerCase());
    }
    
    public boolean isChanRegistered(String channel) {
        return channels.containsKey(channel.toLowerCase());
    }
    
    public Set<String> getCurrentChannels() {
        return currentChannels;
    }

    public synchronized static void main(String[] args) {
        URL path = ClassLoader.getSystemResource(configFile);
        if (path == null) {
            Log.err("Config file not found.  Quitting.");
            return;
        }

        try {
            File fXmlFile = new File(path.toURI());
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = dBuilder.parse(fXmlFile);
            document.getDocumentElement().normalize();

            NodeList nodeList = document.getElementsByTagName("bot");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Attr attributeID = document.createAttribute("id");
                attributeID.setValue(String.valueOf(i));
                nodeList.item(i).getAttributes().setNamedItem(attributeID);
            }

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(path.toURI()));
            Source input = new DOMSource(document);
            transformer.transform(input, output);

            for (int i = 0; i < nodeList.getLength(); i++) {
                Core bot = new Core();
                bot.config = new Configuration((short) i, fXmlFile);

                if (!bot.config.load()) {
                    Log.err("Failed to load config file.  Quitting.");
                    return;
                }

                bot.setDBName(bot.config.getProp("db").getValue());
                bot.db = new DataObjects("jdbc:h2:" + bot.getDBName());

                try {
                    bot.db.initDatabase();
                } catch (Exception e) {
                    Log.err("Failed to initialize database.  Quitting.");
                    return;
                }

                try {
                    bot.setNick(bot.config.getProp("nick").getValue());
                    bot.setNSPass(bot.config.getProp("nspass").getValue());
                    bot.setNSIdent(bot.config.getProp("nsident").getValue());
                    bot.setExt(bot.config.getProp("nick").getAttr("ext"));
                    bot.setUser(bot.config.getProp("username").getValue());
                    bot.setName(bot.config.getProp("realName").getValue());
                    bot.setServer(bot.config.getProp("server").getValue(), Integer.valueOf(bot.config.getProp("port").getValue()));
                    bot.setCmdPrefix(bot.config.getProp("cmdPrefix").getValue());
                    bot.setChanReclaimDuration(Long.valueOf(bot.config.getProp("chanReclaim").getValue()));
                } catch (NumberFormatException e) {
                    Log.err("Invalid numeric types in config file.  Quitting.");
                    return;
                }

                ArrayList<Property> chanList = bot.config.getList("channel");
                if (chanList != null) {
                    for (int j = 0; j < chanList.size(); j++) {
                        String chan = chanList.get(j).getValue();
                        if (chan.indexOf("#") == -1)
                            chan = "#" + chan;

                        bot.addChannel(chan, chanList.get(j).getAttr("key"));
                    }
                }

                ArrayList<Property> adminList = bot.config.getList("admin");
                if (adminList != null) {
                    for (int j = 0; j < adminList.size(); j++)
                        bot.addAdmin(adminList.get(j).getValue());
                }

                ArrayList<Property> cmdList = bot.config.getList("command");
                String[] cmdArr = new String[cmdList.size()];
                for (int j = 0; j < cmdList.size(); j++)
                    cmdArr[j] = cmdList.get(j).getAttr("package") + ".Command" + cmdList.get(j).getValue();
                bot.initEventManager(cmdArr);

                bots.add(bot);
                bot.start();
            }
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (MissingConfigurationFieldException e) {
            e.printStackTrace();
        } catch (MissingConfigurationAttributeException e) {
            e.printStackTrace();
        }
    }

    protected void start() {
        new Thread(this).start();
        this.starttime = System.currentTimeMillis();

        try {
            this.server.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stop() {
        // TODO: Shutdown and cleanup code here.
        server.markDead();
        keepExecuting = false;
    }

    @Override
    public void run() {
        boolean flag = false;
        while (true) {
            flag = !flag;
            Message inMsg;
            while (server.isAlive()) {
                try {
                    while ((inMsg = io().read()) != null) {
                        processServerMessage(inMsg);

                        if (!server.isAlive())
                            break;
                        
                        if (System.currentTimeMillis() - this.getChanReclaimDuration()  > this.lastChanReclaim) {
                            server.joinListedChannels();
                            this.lastChanReclaim = System.currentTimeMillis();
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            while (keepExecuting && !server.isAlive()) {
                try {
                    Thread.sleep(5000);
                    server.connect();

                } catch (InterruptedException e) {
                    keepExecuting = false;
                    e.printStackTrace();
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (!keepExecuting) {
                return;
            }
        }
    }

    private void processServerMessage(Message message) throws IOException {
        switch (message.command) {
            case Irc.PRIVMSG: {
                String target = message.arguments[0];
                String source = message.prefix.substring(0, message.prefix.indexOf("!"));
                String text = message.trailing;

                if (target.equals(getCurrNick()))
                    evnt().onEvent(new EventPrivMsg(source, text));
                else
                    evnt().onEvent(new EventMessage(source, target, text));
                break;
            }
            case Irc.ERR_NICKNAMEINUSE: {
                if (!server.isConnectionComplete()) {
                    server.markDead();

                    String nickToTry = nick;
                    if (useAlt)
                        nickToTry = altnick;
                    nickToTry += new String(new char[++extCount]).replace("\0", ext);

                    if (extCount == 3) {
                        if (useAlt || altnick == null) {
                            Log.info("Could not find successful nick.");
                            return;
                        }

                        extCount = 0;
                        useAlt = true;
                    }

                    server.connect(nickToTry);
                } else {
                    io().message(Irc.NS, Irc.NS_GHOST + " " + nick + " " + nspass);
                    io().writeRaw(Irc.NICK + " " + getNick());
                    io().message(Irc.NS, Irc.NS_ID + " " + nick + " " + nspass);
                }
                break;
            }
            case Irc.RPL_ENDOFMOTD: {
                if (!server.isConnectionComplete()) {
                    io().message(Irc.NS, Irc.NS_ID + " " + getCurrNick() + " " + getNSPass());
                    server.joinListedChannels();
                    server.markConnectionComplete();
                }
                break;
            }
            case Irc.PING: {
                io().writeRaw(Irc.PONG + " " + message.raw.substring(5));
                evnt().onEvent(new EventPing(message.raw.substring(5)));
                break;
            }
            case Irc.RPL_WHOISUSER:
            case Irc.RPL_WHOISSERVER:
            case Irc.RPL_WHOISOPERATOR:
            case Irc.RPL_WHOISIDLE:
            case Irc.RPL_ENDOFWHOIS:
            case Irc.RPL_WHOISCHANNELS:
            case Irc.RPL_WHOISACCOUNT:
            case Irc.RPL_WHOISSECURE:
            case Irc.RPL_WHOISSTAFF:
            case Irc.RPL_WHOISLANGUAGE: {
                String command = message.command;
                String nick = message.arguments[0];
                String[] parameters = Arrays.copyOfRange(message.arguments, 1, message.arguments.length);
                String text = message.trailing;
                evnt.onEvent(new EventWhoIs(command, nick, parameters, text));
                break;
            }
            case Irc.RPL_WHOREPLY: {
                String user = message.arguments[1].substring(1);
                String host = message.arguments[2];
                String nick = message.arguments[3];
                String account = message.arguments[4];
                String name = message.trailing;
                evnt.onEvent(new EventWho(user, host, nick, account, name));
                break;
            }
            case Irc.JOIN: {
                String nick = message.prefix.substring(0, message.prefix.indexOf("!"));
                String channel = message.arguments[0];
                if (nick.equals(getCurrNick())) {
                    currentChannels.add(channel.toLowerCase());
                    evnt.onEvent(new EventSelfJoin(channel));
                } else {
                    String user = message.prefix.substring(message.prefix.indexOf("~") + 1, message.prefix.indexOf("@"));
                    String host = message.prefix.substring(message.prefix.indexOf("@") + 1);
                    String account = message.arguments[1];
                    String name = message.trailing;
                    evnt.onEvent(new EventJoin(user, host, nick, account, name, channel));
                }
                break;
            }
            case Irc.PART: {
                String nick = message.prefix.substring(0, message.prefix.indexOf("!"));
                String channel = message.arguments[0];
                String text = message.trailing;
                if (nick.equals(getCurrNick())) {
                    currentChannels.remove(channel.toLowerCase());
                    evnt.onEvent(new EventSelfPart(channel.toLowerCase()));
                } else {
                    evnt.onEvent(new EventPart(nick, channel, text));
                }
                break;
            }
            case Irc.QUIT: {
                String nick = message.prefix.substring(0, message.prefix.indexOf("!"));
                String text = message.trailing;
                evnt.onEvent(new EventQuit(nick, text));
                break;
            }
            case Irc.KICK: {
                String kicker = message.prefix.substring(0, message.prefix.indexOf("!"));
                String channel = message.arguments[0];
                String nick = message.arguments[1];
                String reason = message.trailing;
                if (nick.equals(getCurrNick())) {
                    currentChannels.remove(channel.toLowerCase());
                    evnt.onEvent(new EventSelfKick(kicker, channel, reason));
                } else {
                    evnt.onEvent(new EventKick(kicker, channel, nick, reason));
                }
                break;
            }
            case Irc.NICK: {
                String nickOld = message.prefix.substring(0, message.prefix.indexOf("!"));
                String nickNew = message.trailing;
                evnt.onEvent(new EventNick(nickOld, nickNew));
                break;
            }
            case Irc.RPL_NAMREPLY: {
                String[] nicks = message.trailing.split(" ");
                String channel = message.arguments[2];

                for (int i = 0; i < nicks.length; i++) {
                    nicks[i] = nicks[i].replace("@", "");
                    nicks[i] = nicks[i].replace("+", "");
                }

                evnt.onEvent(new EventNames(nicks, channel));
                break;
            }
            case Irc.RPL_ENDOFNAMES: {
                String channel = message.arguments[1];
                evnt.onEvent(new EventEndOfNames(channel));
                break;
            }
            case Irc.ERR_NOSUCHCHANNEL:
            case Irc.ERR_TOOMANYCHANNELS:
            case Irc.ERR_INVITEONLYCHAN:
            case Irc.ERR_BANNEDFROMCHAN:
            case Irc.ERR_BADCHANNELKEY: {
                String channel = message.arguments[1];
                this.removeChannel(channel);
                break;
            }
            default:
                break;
        }
    }

    private void initEventManager(String[] commands) {
        ListenerCommand cmdListener = new ListenerCommand(this);

        for (String cmdSet : commands)
            cmdListener.loadCommandSet(cmdSet);

        evnt.addListener(cmdListener);
        evnt.addListener(new ListenerUser(this));
        evnt.addListener(new ListenerFactoid(this));
        
        evnt.init(this);
    }

}
