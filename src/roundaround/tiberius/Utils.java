package roundaround.tiberius;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class Utils {
    public static class User {
        private String nick;
        private String user = null;
        private String host = null;
        private String name = null;
        private String account = null;

        public User(String user, String host, String nick, String account, String name) {
            this.nick = nick;
            this.user = user;
            this.host = host;
            if (!account.equals("0") && !account.equals("*"))
                this.account = account;
            this.name = name;
        }

        public String getNick() {
            return nick;
        }

        public void setNick(String nick) {
            this.nick = nick;
        }

        public String getUsername() {
            return user;
        }

        public void setUsername(String user) {
            this.user = user;
        }

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public boolean isLoggedIn() {
            return account != null;
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(nick) + Objects.hashCode(account);
        }

        @Override
        public boolean equals(Object o) {
            if (o == null)
                return false;
            if (!(o instanceof User))
                return false;

            User u = (User) o;

            if ((this.account == null && u.getAccount() != null) || (this.account != null && u.getAccount() == null))
                return false;

            if (this.account == null && u.getAccount() == null)
                return this.nick.equals(u.getNick());

            return this.account.equals(u.getAccount());
        }

        @Override
        public String toString() {
            return "User [nick=" + nick + ", user=" + user + ", host=" + host + ", name=" + name + ", account=" + account + "]";
        }
    }

    public static class Pair<L, R> {
        private final L left;
        private final R right;

        public Pair(L left, R right) {
            this.left = left;
            this.right = right;
        }

        public L getLeft() {
            return left;
        }

        public R getRight() {
            return right;
        }

        @Override
        public int hashCode() {
            return left.hashCode() ^ right.hashCode();
        }

        @Override
        public boolean equals(Object o) {
            if (o == null)
                return false;
            if (!(o instanceof Pair))
                return false;
            Pair<?, ?> pairo = (Pair<?, ?>) o;
            return this.left.equals(pairo.getLeft()) && this.right.equals(pairo.getRight());
        }

        @Override
        public String toString() {
            return "Pair [left=" + left + ", right=" + right + "]";
        }
    }

    public static class Timespan {
        public final long millis;
        public final long seconds;
        public final long minutes;
        public final long hours;
        public final long days;

        public Timespan(long start, long end) {
            long temp_millis = end - start;

            this.days = TimeUnit.MILLISECONDS.toDays(temp_millis);
            temp_millis -= TimeUnit.DAYS.toMillis(days);

            this.hours = TimeUnit.MILLISECONDS.toHours(temp_millis);
            temp_millis -= TimeUnit.HOURS.toMillis(hours);

            this.minutes = TimeUnit.MILLISECONDS.toMinutes(temp_millis);
            temp_millis -= TimeUnit.MINUTES.toMillis(minutes);

            this.seconds = TimeUnit.MILLISECONDS.toSeconds(temp_millis);
            temp_millis -= TimeUnit.SECONDS.toMillis(seconds);

            this.millis = temp_millis;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (int) (days ^ (days >>> 32));
            result = prime * result + (int) (hours ^ (hours >>> 32));
            result = prime * result + (int) (millis ^ (millis >>> 32));
            result = prime * result + (int) (minutes ^ (minutes >>> 32));
            result = prime * result + (int) (seconds ^ (seconds >>> 32));
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Timespan other = (Timespan) obj;
            if (days != other.days)
                return false;
            if (hours != other.hours)
                return false;
            if (millis != other.millis)
                return false;
            if (minutes != other.minutes)
                return false;
            if (seconds != other.seconds)
                return false;
            return true;
        }

        @Override
        public String toString() {
            return String.format("%d:%02d:%02d:%02d:%03d", days, hours, minutes, seconds, millis);
        }
    }

    public static String toCamelCase(String s) {
        String[] parts = s.split(" ");
        String camelCaseString = "";
        for (String part : parts) {
            camelCaseString = camelCaseString + toProperCase(part);
        }
        return camelCaseString;
    }

    public static String toProperCase(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase() + " ";
    }
}
