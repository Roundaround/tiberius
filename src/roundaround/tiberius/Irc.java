package roundaround.tiberius;

// https://www.alien.net.au/irc/irc2numerics.html

public final class Irc {
	// basics
	public static final String PING    = "PING";
	public static final String PONG    = "PONG";
	public static final String JOIN    = "JOIN";
	public static final String MODE    = "MODE";
	public static final String QUIT    = "QUIT";
	public static final String NICK    = "NICK";
	public static final String USER    = "USER";
	public static final String PRIVMSG = "PRIVMSG";
	public static final String PART    = "PART";
	public static final String TOPIC   = "TOPIC";
	public static final String KICK    = "KICK";
	public static final String NOTICE  = "NOTICE";
	public static final String WHO     = "WHO";
	public static final String WHOIS   = "WHOIS";
	
    // error replies
	public static final String ERR_NOSUCHCHANNEL    = "404";
    public static final String ERR_TOOMANYCHANNELS  = "405";
    public static final String ERR_NONICKNAMEGIVEN  = "431";
    public static final String ERR_ERRONEUSNICKNAME = "432";
    public static final String ERR_NICKNAMEINUSE    = "433";
    public static final String ERR_UNAVAILRESOURCE  = "437";
    public static final String ERR_CHANNELISFULL    = "471";
    public static final String ERR_INVITEONLYCHAN   = "473";
    public static final String ERR_BANNEDFROMCHAN   = "474";
    public static final String ERR_BADCHANNELKEY    = "475";

    // command responses
    public static final String RPL_WELCOME          = "001";
    public static final String RPL_YOURHOST         = "002";
    public static final String RPL_CREATED          = "003";
    public static final String RPL_MYINFO           = "004";

    public static final String RPL_MOTD             = "372";
    public static final String RPL_MOTDSTART        = "375";
    public static final String RPL_ENDOFMOTD        = "376";
    
    public static final String RPL_BANLIST          = "367";
    public static final String RPL_ENDOFBANLIST     = "368";
    
    public static final String RPL_NAMREPLY         = "353";
    
    public static final String RPL_ENDOFNAMES       = "366";
    
    public static final String RPL_WHOISUSER        = "311";
    public static final String RPL_WHOISSERVER      = "312";
    public static final String RPL_WHOISOPERATOR    = "313";
    public static final String RPL_WHOWASUSER       = "314";
    public static final String RPL_ENDOFWHO         = "315";
    public static final String RPL_WHOISIDLE        = "317";
    public static final String RPL_ENDOFWHOIS       = "318";
    public static final String RPL_WHOISCHANNELS    = "319";
    public static final String RPL_WHOISACCOUNT     = "330";
    public static final String RPL_WHOREPLY         = "354";
    public static final String RPL_WHOWASDETAILS    = "670";
    public static final String RPL_WHOISSECURE      = "671";
    public static final String RPL_WHOISSTAFF       = "689";
    public static final String RPL_WHOISLANGUAGE    = "690";
    
    public static final String RPL_USERHOST         = "302";
    
    public static final String RPL_TOPIC            = "332";
    
    public static final String RPL_CHANNELMODEIS    = "324";
    
    // mode changes
    public static final int MODE_OP            = 1;  // +o
    public static final int MODE_DEOP          = 2;  // -o
    public static final int MODE_VOICE         = 3;  // +v
    public static final int MODE_DEVOICE       = 4;  // -v
    public static final int MODE_BAN           = 5;  // +b
    public static final int MODE_UNBAN         = 6;  // -b
    public static final int MODE_KEY_SET       = 7;  // +k
    public static final int MODE_KEY_REMOVED   = 8;  // -k
    public static final int MODE_INVEX_SET     = 9;  // +I
    public static final int MODE_INVEX_REMOVED = 10; // -I
    public static final int MODE_BANEX_SET     = 11; // +e
    public static final int MODE_BANEX_REMOVED = 12; // -e
    public static final int MODE_LIMIT_SET     = 13; // +l
    public static final int MODE_LIMIT_REMOVED = 14; // -l
    
    // nickserv
    public static final String NS       = "NickServ";
    public static final String NS_GHOST = "GHOST";
    public static final String NS_ID    = "IDENTIFY";
    
    // chanserv
    public static final String CS       = "ChanServ";
}
