package roundaround.tiberius.evnt;

public class EventPrivMsg extends EventBase {
	private String source;
	private String message;
	
	public EventPrivMsg(String source, String message) {
		this.source = source;
		this.message = message;
	}
	
	public String getSource() {
		return source;
	}
	
	public String getMessage() {
		return message;
	}
}
