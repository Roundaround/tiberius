package roundaround.tiberius.evnt;


import java.util.ArrayList;

import roundaround.tiberius.Core;
import roundaround.tiberius.listener.ListenerBase;

public class EventManager {
	private ArrayList<ListenerBase> eventListeners;
	
	public EventManager() {
		eventListeners = new ArrayList<ListenerBase>();
	}
	
	public void addListener(ListenerBase listener) {
		eventListeners.add(listener);
	}
	
	public void init(Core bot) {
        for (ListenerBase listener : eventListeners) {
            listener.init();
        }
	}

	public void onEvent(EventEndOfNames evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}

	public void onEvent(EventJoin evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}

    public void onEvent(EventKick evnt) {
        for (ListenerBase listener : eventListeners) {
            listener.onEvent(evnt);
        }
    }
	
	public void onEvent(EventMessage evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
	
	public void onEvent(EventNames evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
	
	public void onEvent(EventNick evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
	
	public void onEvent(EventNotice evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
	
	public void onEvent(EventPart evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
	
	public void onEvent(EventPing evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
	
	public void onEvent(EventPrivMsg evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
	
	public void onEvent(EventQuit evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
	
	public void onEvent(EventSelfJoin evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
    
    public void onEvent(EventSelfKick evnt) {
        for (ListenerBase listener : eventListeners) {
            listener.onEvent(evnt);
        }
    }
    
    public void onEvent(EventSelfPart evnt) {
        for (ListenerBase listener : eventListeners) {
            listener.onEvent(evnt);
        }
    }
	
	public void onEvent(EventWho evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
	
	public void onEvent(EventWhoIs evnt) {
		for (ListenerBase listener : eventListeners) {
			listener.onEvent(evnt);
		}
	}
}
