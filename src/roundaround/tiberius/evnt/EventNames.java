package roundaround.tiberius.evnt;

public class EventNames extends EventBase {
	private String[] nicks;
	private String channel;
	
	public EventNames(String[] nicks, String channel) {
		this.nicks = nicks;
		this.channel = channel;
	}
	
	public String[] getNicks() {
		return nicks;
	}
	
	public String getChannel() {
		return channel;
	}
}
