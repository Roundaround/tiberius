package roundaround.tiberius.evnt;

public class EventEndOfNames extends EventBase {
	private String channel;
	
	public EventEndOfNames(String channel) {
		this.channel = channel;
	}
	
	public String getChannel() {
		return channel;
	}
}
