package roundaround.tiberius.evnt;

public class EventSelfPart extends EventBase {
    private String channel;

    public EventSelfPart(String channel) {
        this.channel = channel;
    }
    
    public String getChannel() {
        return channel;
    }

}
