package roundaround.tiberius.evnt;

import roundaround.tiberius.evnt.EventBase;

public class EventPing extends EventBase {
	private String trailing;
	
	public EventPing(String trailing) {
		this.trailing = trailing;
	}
	
	public String getTrailing() {
		return trailing;
	}
}
