package roundaround.tiberius.evnt;


public class EventSelfKick extends EventBase {
    private String kicker;
    private String channel;
    private String reason;
    
    public EventSelfKick(String kicker, String channel, String reason) {
        this.kicker = kicker;
        this.channel = channel;
        this.reason = reason;
    }
    
    public String getKicker() {
        return kicker;
    }
    
    public String getChannel() {
        return channel;
    }
    
    public String getReason() {
        return reason;
    }
}
