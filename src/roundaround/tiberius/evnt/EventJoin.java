package roundaround.tiberius.evnt;

import roundaround.tiberius.Utils.User;

public class EventJoin extends EventBase {
	private User user;
	private String channel;
	
	public EventJoin(String user, String host, String nick, String account, String name, String channel) {
		this.user = new User(user, host, nick, account, name);
		this.channel = channel;
	}
	
	public User getUser() {
		return user;
	}
	
	public String getChannel() {
		return channel;
	}
}
