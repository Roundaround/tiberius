package roundaround.tiberius.evnt;

public class EventMessage extends EventBase {
	private String source;
	private String channel;
	private String message;
	
	public EventMessage(String source, String channel, String message) {
		this.source = source;
		this.channel = channel;
		this.message = message;
	}
	
	public String getSource() {
		return source;
	}
	
	public String getChannel() {
		return channel;
	}
	
	public String getMessage() {
		return message;
	}
}
