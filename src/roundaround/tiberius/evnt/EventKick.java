package roundaround.tiberius.evnt;


public class EventKick extends EventBase {
    private String kicker;
    private String channel;
    private String nick;
    private String reason;
    
    public EventKick(String kicker, String channel, String nick, String reason) {
        this.kicker = kicker;
        this.channel = channel;
        this.nick = nick;
        this.reason = reason;
    }
    
    public String getKicker() {
        return kicker;
    }
    
    public String getChannel() {
        return channel;
    }
    
    public String getNick() {
        return nick;
    }
    
    public String getReason() {
        return reason;
    }
}
