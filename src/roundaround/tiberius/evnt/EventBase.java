package roundaround.tiberius.evnt;

public abstract class EventBase {
	protected long timestamp = System.currentTimeMillis();
	
	protected EventBase() {}
	
	public final void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
}
