package roundaround.tiberius.evnt;

public class EventQuit extends EventBase {
	private String nick;
	private String message;
	
	public EventQuit(String nick, String message) {
		this.nick = nick;
		this.message = message;
	}
	
	public String getNick() {
		return nick;
	}
	
	public String getMessage() {
		return message;
	}
}
