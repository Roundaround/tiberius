package roundaround.tiberius.evnt;

public class EventPart extends EventBase {
	private String nick;
	private String channel;
	private String message;
	
	public EventPart(String nick, String channel, String message) {
		this.nick = nick;
		this.channel = channel;
		this.message = message;
	}
	
	public String getNick() {
		return nick;
	}
	
	public String getChannel() {
		return channel;
	}
	
	public String getMessage() {
		return message;
	}
}
