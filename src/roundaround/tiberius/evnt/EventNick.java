package roundaround.tiberius.evnt;

public class EventNick extends EventBase {
	private String nickOld;
	private String nickNew;
	
	public EventNick(String nickOld, String nickNew) {
		this.nickOld = nickOld;
		this.nickNew = nickNew;
	}
	
	public String getNickOld() {
		return nickOld;
	}
	
	public String getNickNew() {
		return nickNew;
	}
}
