package roundaround.tiberius.evnt;

import roundaround.tiberius.Utils.User;

public class EventWho extends EventBase {
	private User user;
	
	public EventWho(String user, String host, String nick, String account, String name) {
		this.user = new User(user, host, nick, account, name);
	}

	public User getUser() {
		return user;
	}
}
