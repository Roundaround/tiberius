package roundaround.tiberius.evnt;

public class EventSelfJoin extends EventBase {
	private String channel;
	
	public EventSelfJoin(String channel) {
		this.channel = channel;
	}
	
	public String getChannel() {
		return channel;
	}
}
