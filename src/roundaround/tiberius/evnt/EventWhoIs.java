package roundaround.tiberius.evnt;

public class EventWhoIs extends EventBase {
	private String command;
	private String nick;
	private String[] parameters;
	private String message;
	
	public EventWhoIs(String command, String nick, String[] parameters, String message) {
		this.command = command;
		this.nick = nick;
		this.parameters = parameters;
		this.message = message;
	}
	
	public String getCommand() {
		return command;
	}
	
	public String getNick() {
		return nick;
	}
	
	public String[] getParameters() {
		return parameters;
	}
	
	public String getMessage() {
		return message;
	}
}
