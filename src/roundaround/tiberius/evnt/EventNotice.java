package roundaround.tiberius.evnt;

public class EventNotice extends EventBase {
	private String message;
	
	public EventNotice(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
}
