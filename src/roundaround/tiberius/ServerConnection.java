package roundaround.tiberius;

import java.io.IOException;
import java.net.Socket;
import java.util.Map;

public class ServerConnection {

    private Core bot;

    private boolean connectionComplete = false;

    private String server = null;
    private int port = 6667;

    private Socket socket;
    private IOManager io;

    private boolean connectionAlive = false;

    public void markDead() {
        connectionAlive = false;
    }

    public boolean isAlive() {
        return connectionAlive;
    }

    public boolean isConnectionComplete() {
        return this.connectionComplete;
    }
    
    public void markConnectionComplete() {
        this.connectionComplete = true;
    }

    public IOManager getIOManager() {
        return io;
    }

    public ServerConnection(Core core, String server) {
        this(core, server, 6667);
    }

    public ServerConnection(Core core, String server, int port) {
        this.bot = core;
        this.server = server;
        this.port = port;
    }

    public void partChannel(String channel) {
        if (!channel.startsWith("#"))
            channel = "#" + channel;

        if (bot.getChannels().containsKey(channel.toLowerCase())) {
            try {
                bot.io().writeRaw(Irc.PART + " " + channel);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            bot.removeChannel(channel);
        }
    }

    public void joinChannel(String channel) {
        joinChannel(channel, null);
    }

    public void joinChannel(String channel, String key) {
        if (!channel.startsWith("#"))
            channel = "#" + channel;

        if (!bot.getChannels().containsKey(channel.toLowerCase())) {
            String cmd = Irc.JOIN + " " + channel;
            if (key != null && key.length() > 0)
                cmd += " " + key;

            try {
                bot.io().writeRaw(cmd);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            bot.addChannel(channel, key);
        }
    }

    public void joinListedChannels() throws IOException {
        for (Map.Entry<String, String> channel : bot.getChannels().entrySet()) {
            if (!bot.getCurrentChannels().contains(channel.getKey())) {
                String cmd = Irc.JOIN + " " + channel.getKey();
                if (channel.getValue() != null && channel.getValue().length() > 0)
                    cmd += " " + channel.getValue();
    
                io.writeRaw(cmd);
            }
        }
    }

    public void connect() throws IOException {
        connect(bot.getNick());
    }

    public void connect(String nick) throws IOException {
        if (!connectionAlive) {
            socket = new Socket(server, port);
            io = new IOManager(bot, socket.getInputStream(), socket.getOutputStream());

            if (socket.isConnected()) {
                io.writeRaw("CAP REQ extended-join");
                io.writeRaw("CAP END");
                io.writeRaw(Irc.NICK + " " + nick);
                io.writeRaw(Irc.USER + " " + bot.getUser() + " \"\" \"\" :" + bot.getName());

                bot.setCurrNick(nick);
                connectionAlive = true;
            }
        }
    }
}
