package roundaround.tiberius.cmd;

import java.io.IOException;

import roundaround.tiberius.Core;
import roundaround.tiberius.Utils;
import roundaround.tiberius.Utils.User;
import roundaround.tiberius.listener.ListenerCommand.ICmdAction;
import roundaround.tiberius.listener.ListenerCommand.ICmdWrapper;


public enum CommandCore implements ICmdWrapper {
    PING(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            char[] pong = {'p','o','n','g'};
            String response = "";
            for (int i = 0; i < 4; i++) {
                if (Character.isUpperCase(cmd.charAt(i)))
                    response += Character.toUpperCase(pong[i]);
                else
                    response += pong[i];
            }
            
            bot.io().message(chan, response);
        }

		@Override
		public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
			exec(bot, src, src.getNick(), cmd, args);
		}
        
        @Override
        public String help() {
            return "Returns a pong message from the bot.";
        }
        
        @Override
        public String[] params() {
        	return null;
        }
        
        @Override
        public boolean requiresAdmin() {
        	return false;
        }
    }),
    
    PONG(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            char[] ping = {'p','i','n','g'};
            String response = "";
            for (int i = 0; i < 4; i++) {
                if (Character.isUpperCase(cmd.charAt(i)))
                    response += Character.toUpperCase(ping[i]);
                else
                    response += ping[i];
            }
            
            bot.io().message(chan, response);
        }

		@Override
		public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
			exec(bot, src, src.getNick(), cmd, args);
		}
        
        @Override
        public String help() {
            return "Returns a ping message from the bot.";
        }
        
        @Override
        public String[] params() {
        	return null;
        }
        
        @Override
        public boolean requiresAdmin() {
        	return false;
        }
    }),
    
    UPTIME(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            bot.io().notice(src.getNick(), "I have been running for " + new Utils.Timespan(bot.getStartTime(), System.currentTimeMillis()));
        }

		@Override
		public void execPriv(Core bot, User src, String cmd, String args) throws IOException {
            bot.io().message(src.getNick(), "I have been running for " + new Utils.Timespan(bot.getStartTime(), System.currentTimeMillis()));
		}
        
        @Override
        public String help() {
            return "";
        }
        
        @Override
        public String[] params() {
        	return null;
        }
        
        @Override
        public boolean requiresAdmin() {
        	return false;
        }
    }),
    RUNTIME(UPTIME),
    
    VERSION(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            bot.io().notice(src.getNick(), "I am " + Core.version);
        }

		@Override
		public void execPriv(Core bot, User src, String cmd, String args) throws IOException {
            bot.io().message(src.getNick(), "I am " + Core.version);
		}
        
        @Override
        public String help() {
            return "Returns information about the bot.";
        }
        
        @Override
        public String[] params() {
        	return null;
        }
        
        @Override
        public boolean requiresAdmin() {
        	return false;
        }
    }),
    VER(VERSION),
    VERS(VERSION),
    ABOUT(VERSION),
    
    JOIN(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            String channel = args.split(" ")[0];
            if (channel == null || channel.isEmpty())
                channel = chan;
            
            String key = null;
            if (args.indexOf(" ") > 0)
                key = args.split(" ")[1];
            
            if (!channel.startsWith("#"))
                channel = "#" + channel;
            
            bot.getServer().joinChannel(channel, key);
        }

        @Override
        public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
            exec(bot, src, src.getNick(), cmd, args);
        }
        
        @Override
        public String help() {
            return "Tells the bot to join the provided channels.";
        }
        
        @Override
        public String[] params() {
            return null;
        }
        
        @Override
        public boolean requiresAdmin() {
            return true;
        }
    }),
    
    PART(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            String channel = args.split(" ")[0];
            if (channel == null || channel.isEmpty())
                channel = chan;
            
            if (!channel.startsWith("#"))
                channel = "#" + channel;
            
            bot.getServer().partChannel(channel);
        }

        @Override
        public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
            exec(bot, src, src.getNick(), cmd, args);
        }
        
        @Override
        public String help() {
            return "Tells the bot to part from the provided channels.";
        }
        
        @Override
        public String[] params() {
            return null;
        }
        
        @Override
        public boolean requiresAdmin() {
            return true;
        }
    }),
    
    CHANLIST(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            bot.io().message(src.getNick(), "Not implemented yet you dork.");
        }

        @Override
        public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
            bot.io().message(src.getNick(), "Not implemented yet you dork.");
        }
        
        @Override
        public String help() {
            return "Lists all the channels currently occupying, even secret channels.";
        }
        
        @Override
        public String[] params() {
            return null;
        }
        
        @Override
        public boolean requiresAdmin() {
            return true;
        }
    }),
    
    QUIT(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            bot.io().message(chan, "Okay, goodbye!");
            bot.stop();
        }

        @Override
        public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
            exec(bot, src, src.getNick(), cmd, args);
        }
        
        @Override
        public String help() {
            return "Forces bot to quit.";
        }
        
        @Override
        public String[] params() {
            return null;
        }
        
        @Override
        public boolean requiresAdmin() {
            return true;
        }
    });
    
    private ICmdAction action;
    
    private CommandCore(ICmdAction action) {
        this.action = action;
    }
    
    private CommandCore(CommandCore command) {
        this.action = command.action;
    }
    
    @Override
    public final boolean requiresAdmin() {
    	return action.requiresAdmin();
    }

    @Override
    public final void exec(Core bot, User src, String chan, String cmd, String args) throws Exception {
        action.exec(bot, src, chan, cmd, args);
    }

	@Override
	public final void execPriv(Core bot, User src, String cmd, String args) throws Exception {
		action.execPriv(bot, src, cmd, args);
	}

    @Override
    public final String help() {
        return action.help();
    }

    @Override
    public final String[] params() {
        return action.params();
    }
    
    @Override
    public void init(Core bot) { }
}
