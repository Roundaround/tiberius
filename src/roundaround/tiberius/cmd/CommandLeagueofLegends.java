package roundaround.tiberius.cmd;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import roundaround.tiberius.Core;
import roundaround.tiberius.Utils;
import roundaround.tiberius.Utils.User;
import roundaround.tiberius.listener.ListenerCommand.ICmdAction;
import roundaround.tiberius.listener.ListenerCommand.ICmdWrapper;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

public enum CommandLeagueofLegends implements ICmdWrapper {
    LOLKING(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String argStr) throws IOException {
            boolean cache = true;
            String region = "na";
            String name = argStr;
            String[] args = argStr.split("\\s");
            if (args.length > 1) {
                if (args[0].equals(".")) {
                    cache = false;
                    argStr = argStr.substring(argStr.indexOf(".") + 2);
                    
                    if (args.length > 2 && args[1].startsWith("[") && args[1].endsWith("]")) {
                        region = args[1].substring(1, args[1].length() - 1);
                        name = argStr.substring(argStr.indexOf(" ") + 1);
                    } else {
                        name = argStr;
                    }
                    
                } else if (args[0].startsWith("[") && args[0].endsWith("]")) {
                    region = args[0].substring(1, args[0].length() - 1);
                    name = argStr.substring(argStr.indexOf(" ") + 1);
                }
            }

            try {
                long summonerId = getSummonerIDFromName(bot, region, name, cache);
                if (summonerId != -1) {
                    String result = name + ": http://www.lolking.net/summoner/" + region + "/" + summonerId;
                    bot.io().message(chan, result);
                } else {
                    bot.io().message(chan, "Summoner '" + name + "' not found for the " + region + " region.");
                }
            } catch (HttpStatusException e) {
                if (e.getStatusCode() == 404)
                    bot.io().message(chan, "Summoner '" + name + "' not found for the " + region + " region.");
                else
                    bot.io().message(chan, "I recieved a " + e.getStatusCode() + " HTTP Status code, and so don't have a valid response.  Please try again!");
            }
        }

        @Override
        public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
            exec(bot, src, src.getNick(), cmd, args);
        }

        @Override
        public String help() {
            return "Returns a URL to the requested League of Legends player's LoLKing page.";
        }

        @Override
        public String[] params() {
            return null;
        }

        @Override
        public boolean requiresAdmin() {
            return false;
        }
    }),
    
    LOLRANK(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String argStr) throws IOException {
            boolean cache = true;
            String region = "na";
            String name = argStr;
            String[] args = argStr.split("\\s");
            if (args.length > 1) {
                if (args[0].equals(".")) {
                    cache = false;
                    argStr = argStr.substring(argStr.indexOf(".") + 2);
                    
                    if (args.length > 2 && args[1].startsWith("[") && args[1].endsWith("]")) {
                        region = args[1].substring(1, args[1].length() - 1);
                        name = argStr.substring(argStr.indexOf(" ") + 1);
                    } else {
                        name = argStr;
                    }
                    
                } else if (args[0].startsWith("[") && args[0].endsWith("]")) {
                    region = args[0].substring(1, args[0].length() - 1);
                    name = argStr.substring(argStr.indexOf(" ") + 1);
                }
            }

            long summonerId;
            try {
                summonerId = getSummonerIDFromName(bot, region, name, cache);
                if (summonerId == -1)
                    bot.io().message(chan, "Summoner '" + name + "' not found for the " + region + " region.");
            } catch (HttpStatusException e) {
                if (e.getStatusCode() == 404)
                    bot.io().message(chan, "Summoner '" + name + "' not found for the " + region + " region.");
                else
                    bot.io().message(chan, "I recieved a " + e.getStatusCode() + " HTTP Status code, and so don't have a valid response.  Please try again!");
                
                return;
            }
            
            try {
                HashSet<LeagueItem> leagueEntries = getLeagueEntryBySummoner(bot, region, summonerId, cache);
                LeagueItem solo = null;
                LeagueItem team5 = null;
                LeagueItem team3 = null;
                for (LeagueItem item : leagueEntries) {
                    if (item.queueType.equals("RANKED_SOLO_5x5"))
                        solo = item;
                    else if (item.queueType.equals("RANKED_TEAM_5x5"))
                        team5 = item;
                    else if (item.queueType.equals("RANKED_TEAM_3x3"))
                        team3 = item;
                }
                
                if (solo != null)
                    bot.io().message(chan, name + ": " + Utils.toCamelCase(solo.tier) + solo.rank + " - " + solo.leaguePoints + "LP");
                else if (team5 != null)
                    bot.io().message(chan, name + ": " + Utils.toCamelCase(team5.tier) + team5.rank + " - " + team5.leaguePoints + "LP (" + Utils.toCamelCase(team5.queueType.replaceAll("_", " ")) + ")");
                else if (team3 != null)
                    bot.io().message(chan, name + ": " + Utils.toCamelCase(team3.tier) + team3.rank + " - " + team3.leaguePoints + "LP (" + Utils.toCamelCase(team3.queueType.replaceAll("_", " ")) + ")");
            } catch (HttpStatusException e) {
                if (e.getStatusCode() == 404)
                    bot.io().message(chan, "League information for summoner '" + name + "' not found for the " + region + " region.");
                else
                    bot.io().message(chan, "I recieved a " + e.getStatusCode() + " HTTP Status code, and so don't have a valid response.  Please try again!");
            }
            
        }

        @Override
        public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
            exec(bot, src, src.getNick(), cmd, args);
        }

        @Override
        public String help() {
            return "Returns the ranked level of the provided League of Legends player.";
        }

        @Override
        public String[] params() {
            return null;
        }

        @Override
        public boolean requiresAdmin() {
            return false;
        }
    }),
    
    LOLNEXUS(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String argStr) throws IOException {
            String region = "na";
            String name = argStr;
            String[] args = argStr.split("\\s");
            if (args.length > 1 && args[0].startsWith("[") && args[0].endsWith("]")) {
                region = args[0].substring(1, args[0].length() - 1);
                name = argStr.substring(argStr.indexOf("\\s") + 1);
            }

            String standardName = name.replaceAll("\\s", "").toLowerCase();
            String requestUrl = "http://www.lolnexus.com/ajax/get-game-info/" + region.toUpperCase() + ".json?name=" + standardName;
            String responseUrl = "http://www.lolnexus.com/" + region + "/search?name=" + standardName;
            
            try {
                LoLNexusResponse response = new Gson().fromJson(Jsoup.connect(requestUrl).ignoreContentType(true).execute().body(), new TypeToken<LoLNexusResponse>() { }.getType());
                if (!response.getSuccessful()) {
                    String errorText = Jsoup.parse(response.getHtml()).select(".error").first().text().replaceAll(standardName, "'" + name + "'") + " (" + responseUrl + ")";
                    bot.io().message(chan, errorText);
                } else {
                    Element wrapper = Jsoup.parse(response.getHtml()).select(".header-bar").first();
                    Element h2 = wrapper.select("h2").first();
                    h2.select("small").remove();
                    String gameType = h2.text();

                    Utils.Timespan startTime = new Utils.Timespan(Long.parseLong(wrapper.select("[data-game-time]").first().attr("data-game-time")) * 1000L, System.currentTimeMillis());
                    String runTime;
                    if (startTime.hours > 0)
                        runTime = String.format("%02d:%02d:%02d", startTime.hours, startTime.minutes, startTime.seconds);
                    else
                        runTime = String.format("%02d:%02d", startTime.minutes, startTime.seconds);
                    
                    bot.io().message(chan, name + ": " + gameType.trim() + " - " + runTime + " (" + responseUrl + ")");
                }
            } catch (HttpStatusException e) {
                bot.io().message(chan, "I recieved a " + e.getStatusCode() + " HTTP Status code, and so don't have a valid response.  Please try again!");
            }
        }

        @Override
        public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
            exec(bot, src, src.getNick(), cmd, args);
        }

        @Override
        public String help() {
            return "Returns a URL to a LoLNexus lookup for the provided League of Legends player.";
        }

        @Override
        public String[] params() {
            return null;
        }

        @Override
        public boolean requiresAdmin() {
            return false;
        }
    });

    private ICmdAction action;

    private CommandLeagueofLegends(ICmdAction action) {
        this.action = action;
    }

    private CommandLeagueofLegends(CommandLeagueofLegends command) {
        this.action = command.action;
    }

    @Override
    public final boolean requiresAdmin() {
        return action.requiresAdmin();
    }

    @Override
    public final void exec(Core bot, User src, String chan, String cmd, String args) throws Exception {
        action.exec(bot, src, chan, cmd, args);
    }

    @Override
    public final void execPriv(Core bot, User src, String cmd, String args) throws Exception {
        action.execPriv(bot, src, cmd, args);
    }

    @Override
    public final String help() {
        return action.help();
    }

    @Override
    public final String[] params() {
        return action.params();
    }
    
    @Override
    public void init(Core bot) { }

    public static final HashMap<String, String> vers = new HashMap<String, String>();
    static {
        vers.put("champion", "1.1");
        vers.put("game", "1.3");
        vers.put("league", "2.3");
        vers.put("static", "1");
        vers.put("stats", "1.2");
        vers.put("summoner", "1.3");
        vers.put("team", "2.2");
    };

    public static final String riotAPIString = "https://prod.api.pvp.net/api/lol/";
    public static final String riotAPIKey = "9f4facdd-a592-44d3-b9c1-d8a0f62c2177";

    public static class Summoner {
        private long id;
        private String name;
        private int profileIconId;
        private long revisionDate;
        private long summonerLevel;
        private String region;

        public Summoner(long id, String name, int profileIconId, long revisionDate, long summonerLevel, String region) {
            this.id = id;
            this.name = name;
            this.profileIconId = profileIconId;
            this.revisionDate = revisionDate;
            this.summonerLevel = summonerLevel;
            this.region = region;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getProfileIconId() {
            return profileIconId;
        }

        public void setProfileIconId(int profileIconId) {
            this.profileIconId = profileIconId;
        }

        public long getRevisionDate() {
            return revisionDate;
        }

        public void setRevisionDate(long revisionDate) {
            this.revisionDate = revisionDate;
        }

        public long getSummonerLevel() {
            return summonerLevel;
        }

        public void setSummonerLevel(long summonerLevel) {
            this.summonerLevel = summonerLevel;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (int) (id ^ (id >>> 32));
            result = prime * result + ((region == null) ? 0 : region.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Summoner other = (Summoner) obj;
            if (id != other.id)
                return false;
            if (region == null) {
                if (other.region != null)
                    return false;
            } else if (!region.equals(other.region))
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "Summoner [id=" + id + ", name=" + name + ", profileIconId=" + profileIconId + ", revisionDate="
                    + revisionDate + ", summonerLevel=" + summonerLevel + ", region=" + region + "]";
        }
    }
    
    public static class League {
        private HashSet<LeagueItem> entries;
        private String name;
        private String participantId;
        private String queue;
        private String tier;
        
        public League(HashSet<LeagueItem> entries, String name, String participantId, String queue, String tier) {
            this.entries = entries;
            this.name = name;
            this.participantId = participantId;
            this.queue = queue;
            this.tier = tier;
        }

        public HashSet<LeagueItem> getEntries() {
            return entries;
        }

        public void setEntries(HashSet<LeagueItem> entries) {
            this.entries = entries;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getParticipantId() {
            return participantId;
        }

        public void setParticipantId(String participantId) {
            this.participantId = participantId;
        }

        public String getQueue() {
            return queue;
        }

        public void setQueue(String queue) {
            this.queue = queue;
        }

        public String getTier() {
            return tier;
        }

        public void setTier(String tier) {
            this.tier = tier;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((entries == null) ? 0 : entries.hashCode());
            result = prime * result + ((name == null) ? 0 : name.hashCode());
            result = prime * result + ((participantId == null) ? 0 : participantId.hashCode());
            result = prime * result + ((queue == null) ? 0 : queue.hashCode());
            result = prime * result + ((tier == null) ? 0 : tier.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            League other = (League) obj;
            if (entries == null) {
                if (other.entries != null)
                    return false;
            } else if (!entries.equals(other.entries))
                return false;
            if (name == null) {
                if (other.name != null)
                    return false;
            } else if (!name.equals(other.name))
                return false;
            if (participantId == null) {
                if (other.participantId != null)
                    return false;
            } else if (!participantId.equals(other.participantId))
                return false;
            if (queue == null) {
                if (other.queue != null)
                    return false;
            } else if (!queue.equals(other.queue))
                return false;
            if (tier == null) {
                if (other.tier != null)
                    return false;
            } else if (!tier.equals(other.tier))
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "LeagueDto [entries=" + entries + ", name=" + name + ", participantId=" + participantId + ", queue="
                    + queue + ", tier=" + tier + "]";
        }
    }
    
    public static class LeagueItem {
        private boolean isFreshBlood;
        private boolean isHotStreak;
        private boolean isInactive;
        private boolean isVeteran;
        private long lastPlayed;
        private String leagueName;
        private int leaguePoints;
        private MiniSeries miniSeries;
        private String playerOrTeamId;
        private String playerOrTeamName;
        private String queueType;
        private String rank;
        private String tier;
        private int wins;
        
        public LeagueItem(boolean isFreshBlood, boolean isHotStreak, boolean isInactive, boolean isVeteran,
                long lastPlayed, String leagueName, int leaguePoints, MiniSeries miniSeries, String playerOrTeamId,
                String playerOrTeamName, String queueType, String rank, String tier, int wins) {
            this.isFreshBlood = isFreshBlood;
            this.isHotStreak = isHotStreak;
            this.isInactive = isInactive;
            this.isVeteran = isVeteran;
            this.lastPlayed = lastPlayed;
            this.leagueName = leagueName;
            this.leaguePoints = leaguePoints;
            this.miniSeries = miniSeries;
            this.playerOrTeamId = playerOrTeamId;
            this.playerOrTeamName = playerOrTeamName;
            this.queueType = queueType;
            this.rank = rank;
            this.tier = tier;
            this.wins = wins;
        }

        public boolean isFreshBlood() {
            return isFreshBlood;
        }

        public void setFreshBlood(boolean isFreshBlood) {
            this.isFreshBlood = isFreshBlood;
        }

        public boolean isHotStreak() {
            return isHotStreak;
        }

        public void setHotStreak(boolean isHotStreak) {
            this.isHotStreak = isHotStreak;
        }

        public boolean isInactive() {
            return isInactive;
        }

        public void setInactive(boolean isInactive) {
            this.isInactive = isInactive;
        }

        public boolean isVeteran() {
            return isVeteran;
        }

        public void setVeteran(boolean isVeteran) {
            this.isVeteran = isVeteran;
        }

        public long getLastPlayed() {
            return lastPlayed;
        }

        public void setLastPlayed(long lastPlayed) {
            this.lastPlayed = lastPlayed;
        }

        public String getLeagueName() {
            return leagueName;
        }

        public void setLeagueName(String leagueName) {
            this.leagueName = leagueName;
        }

        public int getLeaguePoints() {
            return leaguePoints;
        }

        public void setLeaguePoints(int leaguePoints) {
            this.leaguePoints = leaguePoints;
        }

        public MiniSeries getMiniSeries() {
            return miniSeries;
        }

        public void setMiniSeries(MiniSeries miniSeries) {
            this.miniSeries = miniSeries;
        }

        public String getPlayerOrTeamId() {
            return playerOrTeamId;
        }

        public void setPlayerOrTeamId(String playerOrTeamId) {
            this.playerOrTeamId = playerOrTeamId;
        }

        public String getPlayerOrTeamName() {
            return playerOrTeamName;
        }

        public void setPlayerOrTeamName(String playerOrTeamName) {
            this.playerOrTeamName = playerOrTeamName;
        }

        public String getQueueType() {
            return queueType;
        }

        public void setQueueType(String queueType) {
            this.queueType = queueType;
        }

        public String getRank() {
            return rank;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public String getTier() {
            return tier;
        }

        public void setTier(String tier) {
            this.tier = tier;
        }

        public int getWins() {
            return wins;
        }

        public void setWins(int wins) {
            this.wins = wins;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (isFreshBlood ? 1231 : 1237);
            result = prime * result + (isHotStreak ? 1231 : 1237);
            result = prime * result + (isInactive ? 1231 : 1237);
            result = prime * result + (isVeteran ? 1231 : 1237);
            result = prime * result + (int) (lastPlayed ^ (lastPlayed >>> 32));
            result = prime * result + ((leagueName == null) ? 0 : leagueName.hashCode());
            result = prime * result + leaguePoints;
            result = prime * result + ((miniSeries == null) ? 0 : miniSeries.hashCode());
            result = prime * result + ((playerOrTeamId == null) ? 0 : playerOrTeamId.hashCode());
            result = prime * result + ((playerOrTeamName == null) ? 0 : playerOrTeamName.hashCode());
            result = prime * result + ((queueType == null) ? 0 : queueType.hashCode());
            result = prime * result + ((rank == null) ? 0 : rank.hashCode());
            result = prime * result + ((tier == null) ? 0 : tier.hashCode());
            result = prime * result + wins;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            LeagueItem other = (LeagueItem) obj;
            if (isFreshBlood != other.isFreshBlood)
                return false;
            if (isHotStreak != other.isHotStreak)
                return false;
            if (isInactive != other.isInactive)
                return false;
            if (isVeteran != other.isVeteran)
                return false;
            if (lastPlayed != other.lastPlayed)
                return false;
            if (leagueName == null) {
                if (other.leagueName != null)
                    return false;
            } else if (!leagueName.equals(other.leagueName))
                return false;
            if (leaguePoints != other.leaguePoints)
                return false;
            if (miniSeries == null) {
                if (other.miniSeries != null)
                    return false;
            } else if (!miniSeries.equals(other.miniSeries))
                return false;
            if (playerOrTeamId == null) {
                if (other.playerOrTeamId != null)
                    return false;
            } else if (!playerOrTeamId.equals(other.playerOrTeamId))
                return false;
            if (playerOrTeamName == null) {
                if (other.playerOrTeamName != null)
                    return false;
            } else if (!playerOrTeamName.equals(other.playerOrTeamName))
                return false;
            if (queueType == null) {
                if (other.queueType != null)
                    return false;
            } else if (!queueType.equals(other.queueType))
                return false;
            if (rank == null) {
                if (other.rank != null)
                    return false;
            } else if (!rank.equals(other.rank))
                return false;
            if (tier == null) {
                if (other.tier != null)
                    return false;
            } else if (!tier.equals(other.tier))
                return false;
            if (wins != other.wins)
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "LeagueItemDto [isFreshBlood=" + isFreshBlood + ", isHotStreak=" + isHotStreak + ", isInactive="
                    + isInactive + ", isVeteran=" + isVeteran + ", lastPlayed=" + lastPlayed + ", leagueName="
                    + leagueName + ", leaguePoints=" + leaguePoints + ", miniSeries=" + miniSeries
                    + ", playerOrTeamId=" + playerOrTeamId + ", playerOrTeamName=" + playerOrTeamName + ", queueType="
                    + queueType + ", rank=" + rank + ", tier=" + tier + ", wins=" + wins + "]";
        }
    }
    
    public static class MiniSeries {
        private int losses;
        private char[] progress;
        private int target;
        private long timeLeftToPlayMillis;
        private int wins;
        
        public MiniSeries(int losses, char[] progress, int target, long timeLeftToPlayMillis, int wins) {
            this.losses = losses;
            this.progress = progress;
            this.target = target;
            this.timeLeftToPlayMillis = timeLeftToPlayMillis;
            this.wins = wins;
        }

        public int getLosses() {
            return losses;
        }

        public void setLosses(int losses) {
            this.losses = losses;
        }

        public char[] getProgress() {
            return progress;
        }

        public void setProgress(char[] progress) {
            this.progress = progress;
        }

        public int getTarget() {
            return target;
        }

        public void setTarget(int target) {
            this.target = target;
        }

        public long getTimeLeftToPlayMillis() {
            return timeLeftToPlayMillis;
        }

        public void setTimeLeftToPlayMillis(long timeLeftToPlayMillis) {
            this.timeLeftToPlayMillis = timeLeftToPlayMillis;
        }

        public int getWins() {
            return wins;
        }

        public void setWins(int wins) {
            this.wins = wins;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + losses;
            result = prime * result + Arrays.hashCode(progress);
            result = prime * result + target;
            result = prime * result + (int) (timeLeftToPlayMillis ^ (timeLeftToPlayMillis >>> 32));
            result = prime * result + wins;
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            MiniSeries other = (MiniSeries) obj;
            if (losses != other.losses)
                return false;
            if (!Arrays.equals(progress, other.progress))
                return false;
            if (target != other.target)
                return false;
            if (timeLeftToPlayMillis != other.timeLeftToPlayMillis)
                return false;
            if (wins != other.wins)
                return false;
            return true;
        }

        @Override
        public String toString() {
            return "MiniSeriesDto [losses=" + losses + ", progress=" + Arrays.toString(progress) + ", target=" + target
                    + ", timeLeftToPlayMillis=" + timeLeftToPlayMillis + ", wins=" + wins + "]";
        }
    }
    
    public static class LoLNexusResponse {
        private boolean successful;
        private String html;
        
        public LoLNexusResponse(boolean successful, String html) {
            this.successful = successful;
            this.html = html;
        }
        
        public boolean getSuccessful() {
            return successful;
        }
        
        public void setSuccessful(boolean successful) {
            this.successful = successful;
        }
        
        public String getHtml() {
            return html;
        }
        
        public void setHtml(String html) {
            this.html = html;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((html == null) ? 0 : html.hashCode());
            result = prime * result + (successful ? 1231 : 1237);
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            LoLNexusResponse other = (LoLNexusResponse) obj;
            if (html == null) {
                if (other.html != null)
                    return false;
            } else if (!html.equals(other.html))
                return false;
            if (successful != other.successful)
                return false;
            return true;
        }
    }

    public static String buildRiotAPIURL(String region, String api, String query) {
        return riotAPIString + region + "/v" + vers.get(api) + "/" + api + "/" + query + "?api_key=" + riotAPIKey;
    }

    public static Summoner getSummonerByName(Core bot, String region, String name, boolean cache) throws HttpStatusException {
        String standardName = name.replaceAll("\\s", "").toLowerCase();
        String searchKey = "SummonerByName_" + standardName + "[" + region + "]";
        if (cache && bot.isCached(searchKey)) {
            if (bot.getCache(searchKey) instanceof Summoner && System.currentTimeMillis() < bot.getCachedDate(searchKey) + 1800000L) // 30 min
                return (Summoner) bot.getCache(searchKey);
            bot.removeCache(searchKey);
        }
        
        String requestURL = buildRiotAPIURL(region, "summoner", "by-name/" + standardName);
        
        try {
            HashMap<String, Summoner> result = new Gson().fromJson(Jsoup.connect(requestURL).ignoreContentType(true).execute().body(), new TypeToken<HashMap<String, Summoner>>() { }.getType());

            if (result.isEmpty() || !result.containsKey(standardName))
                return null;

            Summoner summoner = result.get(standardName);
            summoner.setRegion(region);

            if (cache) {
                bot.putCache(searchKey, summoner);
                bot.putCache("SummonerNameToID_" + standardName, summoner.id);
            }

            return summoner;
            
        } catch (HttpStatusException e) {
            throw e;
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        // If we get here, something went wrong, so we return a null value and report error to user in command execution code.
        return null;
    }
    
    public static long getSummonerIDFromName(Core bot, String region, String name, boolean cache) throws HttpStatusException {
        String standardName = name.replaceAll("\\s", "").toLowerCase();
        String searchKey = "SummonerNameToID_" + standardName + "[" + region + "]";
        if (cache && bot.isCached(searchKey)) {
            if (bot.getCache(searchKey) instanceof Long && System.currentTimeMillis() < bot.getCachedDate(searchKey) + 86400000L) // 24 hrs
                return (long) bot.getCache(searchKey);
            bot.removeCache(searchKey);
        }
        
        try {
            Summoner summoner = getSummonerByName(bot, region, name, cache);
            
            if (summoner == null) {
                // If we get here, something went wrong, so we return a null value and report error to user in command execution code.
                return -1;
            }
            
            if (cache)
                bot.putCache(searchKey, summoner.id);
            
            return summoner.id;
            
        } catch (HttpStatusException e) {
            throw e;
        }
    }
    
    @SuppressWarnings("unchecked")
    public static HashSet<LeagueItem> getLeagueEntryBySummoner(Core bot, String region, long id, boolean cache) throws HttpStatusException {
        String searchKey = "LeagueEntryBySummoner_" + id + "[" + region + "]";
        if (cache && bot.isCached(searchKey)) {
            if (bot.getCache(searchKey) instanceof HashSet<?> && System.currentTimeMillis() < bot.getCachedDate(searchKey) + 1800000L) // 30 min
                return (HashSet<LeagueItem>) bot.getCache(searchKey);
            bot.removeCache(searchKey);
        }
        
        String requestURL = buildRiotAPIURL(region, "league", "by-summoner/" + String.valueOf(id) + "/entry");
        
        try {
            HashSet<LeagueItem> result = new Gson().fromJson(Jsoup.connect(requestURL).ignoreContentType(true).execute().body(), new TypeToken<HashSet<LeagueItem>>() { }.getType());

            if (result.isEmpty())
                return null;

            if (cache)
                bot.putCache(searchKey, result);

            return result;
        } catch (HttpStatusException e) {
            throw e;
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        // If we get here, something went wrong, so we return a null value and report error to user in command execution code.
        return null;
    }

}
