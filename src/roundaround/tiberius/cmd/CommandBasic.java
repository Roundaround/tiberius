package roundaround.tiberius.cmd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import roundaround.tiberius.Core;
import roundaround.tiberius.Utils.User;
import roundaround.tiberius.listener.ListenerCommand.ICmdAction;
import roundaround.tiberius.listener.ListenerCommand.ICmdWrapper;

public enum CommandBasic implements ICmdWrapper {
    CHOOSE(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            if (args.length() > 0) {
                String[] rawChoices;
                if (args.indexOf(",") > 0) {
                    rawChoices = args.split(",");
                } else if (args.indexOf(" ") > 0) {
                    rawChoices = args.split(" ");
                } else {
                    rawChoices = new String[] { args };
                }
                
                ArrayList<String> choices = new ArrayList<String>();
                for (int i = 0; i < rawChoices.length; i++) {
                    if (!rawChoices[i].trim().isEmpty())
                        choices.add(rawChoices[i].trim());
                }
                
                Random rand = new Random();
                bot.io().message(chan, choices.get(rand.nextInt(choices.size())));
            } else {
                bot.io().message(chan, "Sand blasted grease monkeys!");
            }
        }

        @Override
        public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
            exec(bot, src, src.getNick(), cmd, args);
        }
        
        @Override
        public String help() {
            return "Chooses a random item from the given options.";
        }
        
        @Override
        public String[] params() {
            return null;
        }
        
        @Override
        public boolean requiresAdmin() {
            return false;
        }
    }),
    
    ROULETTE(new ICmdAction() {
        @Override
        public void exec(Core bot, User src, String chan, String cmd, String args) throws IOException {
            bot.io().message(chan, "Not implemented yet you dork.");
        }

        @Override
        public void execPriv(Core bot, User src, String cmd, String args) throws Exception {
            exec(bot, src, src.getNick(), cmd, args);
        }
        
        @Override
        public String help() {
            return "Pulls the trigger on a memory-utilizing roulette game.";
        }
        
        @Override
        public String[] params() {
            return null;
        }
        
        @Override
        public boolean requiresAdmin() {
            return false;
        }
    });
    
    private ICmdAction action;
    
    private CommandBasic(ICmdAction action) {
        this.action = action;
    }
    
    private CommandBasic(CommandBasic command) {
        this.action = command.action;
    }
    
    @Override
    public final boolean requiresAdmin() {
        return action.requiresAdmin();
    }

    @Override
    public final void exec(Core bot, User src, String chan, String cmd, String args) throws Exception {
        action.exec(bot, src, chan, cmd, args);
    }

    @Override
    public final void execPriv(Core bot, User src, String cmd, String args) throws Exception {
        action.execPriv(bot, src, cmd, args);
    }

    @Override
    public final String help() {
        return action.help();
    }

    @Override
    public final String[] params() {
        return action.params();
    }
    
    @Override
    public void init(Core bot) { }

}
