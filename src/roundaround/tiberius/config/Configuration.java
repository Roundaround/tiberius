package roundaround.tiberius.config;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import roundaround.tiberius.exception.MissingConfigurationAttributeException;
import roundaround.tiberius.exception.MissingConfigurationFieldException;

public class Configuration {
    
    private short botID;
    private File file = null;
    
    private MultiHashMap<String, Property> properties = new MultiHashMap<String, Property>();
    
    // TODO: Move field and attribute lists outside configuration file.  Pass them in instead.
    private String[] requiredFields = {
        "nick", "server"
    };
    private String[] listFields = {
        "channel", "admin", "command"
    };
    private HashMap<String, String> optionalFields = new HashMap<String, String>();
    {
        optionalFields.put("db", "Tiberius");
        optionalFields.put("altnick", null);
        optionalFields.put("nspass", null);
        optionalFields.put("nsident", "<nick>");
        optionalFields.put("username", "<nick>");
        optionalFields.put("realName", "<nick>");
        optionalFields.put("port", "6667");
        optionalFields.put("cmdPrefix", ".");
        optionalFields.put("fctdPrefix", "~");
        optionalFields.put("chanReclaim", "30000");
    }
    
    private String[] requiredAttributes = { };
    private HashMap<String, String> optionalAttributes = new HashMap<String, String>();
    {
        optionalAttributes.put("channel.key", null);
        optionalAttributes.put("nick.ext", "_");
        optionalAttributes.put("command.package", "roundaround.tiberius.cmd");
    }
    
    public Configuration(short botID, File file) {
        this.botID = botID;
        this.file = file;
    }
    
    public synchronized boolean load() throws MissingConfigurationFieldException, MissingConfigurationAttributeException {
        if (this.file == null) {
            System.err.println("Config file is null!");
            return false;
        }
        
        int extIndex = this.file.getPath().lastIndexOf(".");
        if (extIndex == -1 || !this.file.getPath().substring(extIndex + 1).equalsIgnoreCase("xml")) {
            System.err.println("Config file is not an xml file!");
            return false;
        }
        
        try {
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(this.file);
            document.getDocumentElement().normalize();
            
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile("//bot[@id=\"" + this.botID + "\"][1]");
            
            Element botElem = (Element)expr.evaluate(document, XPathConstants.NODE);
            for (int i = 0; i < requiredFields.length; i++) {
                String field = this.requiredFields[i];
                
                if (botElem == null)
                    return false;
                
                Node fieldNode = botElem.getElementsByTagName(field).item(0);
                if (fieldNode == null)
                    throw new MissingConfigurationFieldException(field);
                
                Property property = new Property(fieldNode.getTextContent());
                
                HashMap<String, String> attrMap = new HashMap<String, String>();
                NamedNodeMap attributes = fieldNode.getAttributes();
                for (int j = 0; j < attributes.getLength(); j++) {
                    Node attrNode = attributes.item(j);
                    attrMap.put(attrNode.getNodeName(), attrNode.getTextContent());
                }
                
                property = attachAttributesToProperty(property, attrMap, field);

                properties.put(field, property);
            }
            
            for (int i = 0; i < listFields.length; i++) {
                String field = this.listFields[i];
                
                NodeList fieldNodes = botElem.getElementsByTagName(field);
                for (int j = 0; j < fieldNodes.getLength(); j++) {
                    Node fieldNode = fieldNodes.item(j);
                    
                    Property property = new Property(fieldNode.getTextContent());
                    
                    HashMap<String, String> attrMap = new HashMap<String, String>();
                    NamedNodeMap attributes = fieldNode.getAttributes();
                    for (int k = 0; k < attributes.getLength(); k++) {
                        Node attrNode = attributes.item(k);
                        attrMap.put(attrNode.getNodeName(), attrNode.getTextContent());
                    }
                    
                    property = attachAttributesToProperty(property, attrMap, field);

                    properties.put(field, property);
                }
            }

            Iterator<Entry<String, String>> it = optionalFields.entrySet().iterator();
            while (it.hasNext()) {
                Entry<String, String> pair = it.next();
                
                String field = pair.getKey();
                String value = pair.getValue();
                
                Node fieldNode = botElem.getElementsByTagName(field).item(0);
                if (fieldNode != null)
                    value = fieldNode.getTextContent();
                
                if (pair.getValue() != null && pair.getValue().startsWith("<") && pair.getValue().endsWith(">") && !pair.getValue().equals("<>")) {
                    String fetch = pair.getValue().substring(1, pair.getValue().length() - 1);
                    ArrayList<Property> fetchList = properties.get(fetch);
                    if (fetchList != null && !fetchList.isEmpty())
                        value = fetchList.get(0).getValue();
                }
                
                Property property = new Property(value);
                
                if (fieldNode != null) {
                    HashMap<String, String> attrMap = new HashMap<String, String>();
                    NamedNodeMap attributes = fieldNode.getAttributes();
                    for (int k = 0; k < attributes.getLength(); k++) {
                        Node attrNode = attributes.item(k);
                        attrMap.put(attrNode.getNodeName(), attrNode.getTextContent());
                    }
                    
                    property = attachAttributesToProperty(property, attrMap, field);
                }
                
                properties.put(field, property);
            }
            
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XPathExpressionException e) {
            e.printStackTrace();
        }
        
        return true;
    }
    
    private Property attachAttributesToProperty(Property property, HashMap<String, String> attrMap, String field) throws MissingConfigurationAttributeException {
        String[] requiredAttributesForNode = getRequiredAttributes(field);
        for (int j = 0; j < requiredAttributesForNode.length; j++) {
            String attrName = requiredAttributesForNode[j];
            String attrVal = attrMap.get(attrName);
            if (attrVal == null)
                throw new MissingConfigurationAttributeException(field, requiredAttributesForNode[j]);

            property.setAttr(attrName, attrVal);
        }
        
        HashMap<String, String> optionalAttributesForNode = getOptionalAttributes(field);
        Iterator<Entry<String, String>> it = optionalAttributesForNode.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, String> pair = it.next();
            String attrName = pair.getKey();
            String attrVal = attrMap.get(attrName);
            if (attrVal == null)
                attrVal = pair.getValue();
            
            property.setAttr(attrName, attrVal);
        }
        
        return property;
    }
    
    private String[] getRequiredAttributes(String nodeName) {
        HashSet<String> attrSet = new HashSet<String>();
        
        for (int i = 0; i < this.requiredAttributes.length; i++) {
            String attr = this.requiredAttributes[i];
            
            if (attr.startsWith(nodeName + "."))
                attrSet.add(attr.substring(nodeName.length() + 1));
        }
        
        return attrSet.toArray(new String[0]);
    }
    
    private HashMap<String, String> getOptionalAttributes(String nodeName) {
        HashMap<String, String> attrMap = new HashMap<String, String>();
        
        Iterator<Entry<String, String>> it = this.optionalAttributes.entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, String> pair = it.next();
            String attr = pair.getKey();
            
            if (attr.startsWith(nodeName + "."))
                attrMap.put(attr.substring(nodeName.length() + 1), pair.getValue());
        }
        
        return attrMap;
    }
    
    public synchronized boolean save() throws IOException {
        
        return false;
    }
    
    public Property getProp(String key) {
        ArrayList<Property> get = properties.get(key);
        if (get == null || get.size() != 1)
            return null;
        return get.get(0);
    }
    
    public ArrayList<Property> getList(String key) {
        return properties.get(key);
    }
}
