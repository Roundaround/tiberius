package roundaround.tiberius.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A wrapper class containing a HashMap of ArrayLists.
 * 
 * @author Evan Steinkerchner
 *
 * @param <K> Key Type
 * @param <V> Value Type
 */
public class MultiHashMap<K, V> {
    
    private HashMap<K, ArrayList<V>> internalMap;
    
    public MultiHashMap() {
        internalMap = new HashMap<K, ArrayList<V>>();
    }
    
    public ArrayList<V> get(K key) {
        return internalMap.get(key);
    }
    
    public ArrayList<V> put(K key, V value) {
        ArrayList<V> current = internalMap.get(key);
        if (current == null) {
            current = new ArrayList<V>();
            internalMap.put(key, current);
        }
        current.add(value);
        return current;
    }
    
    public void putAll(Map<? extends K, ? extends ArrayList<V>> m) {
        internalMap.putAll(m);
    }
    
    public ArrayList<V> remove(K key) {
        return internalMap.remove(key);
    }
}
