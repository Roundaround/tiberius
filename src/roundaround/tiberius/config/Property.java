package roundaround.tiberius.config;

import java.util.HashMap;

public class Property {

    private String val;
    private HashMap<String, String> attrs;
    
    public Property(String val) {
        this.val = val;
        this.attrs = new HashMap<String, String>();
    }
    
    public void setAttr(String attr, String val) {
        this.attrs.put(attr, val);
    }
    
    public String getAttr(String attr) {
        return this.attrs.get(attr);
    }
    
    public void setValue(String val) {
        this.val = val;
    }
    
    public String getValue() {
        return val;
    }
}
