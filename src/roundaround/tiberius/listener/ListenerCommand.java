package roundaround.tiberius.listener;

import java.util.ArrayList;

import roundaround.tiberius.Core;
import roundaround.tiberius.Utils.User;
import roundaround.tiberius.evnt.EventMessage;
import roundaround.tiberius.evnt.EventPrivMsg;

public class ListenerCommand extends ListenerBase {

    private ArrayList<Class<? extends ICmdWrapper>> commands = new ArrayList<Class<? extends ICmdWrapper>>();
    
    public ListenerCommand(Core bot) {
        super(bot);
    }

    public interface ICmdAction {
        public boolean requiresAdmin();

        public void exec(Core bot, User src, String chan, String cmd, String args) throws Exception;

        public void execPriv(Core bot, User src, String cmd, String args) throws Exception;

        public String help();

        public String[] params();
    }

    public interface ICmdWrapper {
        public boolean requiresAdmin();

        public void exec(Core bot, User src, String chan, String cmd, String args) throws Exception;

        public void execPriv(Core bot, User src, String cmd, String args) throws Exception;

        public String help();

        public String[] params();

        public void init(Core bot);
    }

    @SuppressWarnings("unchecked")
    public boolean loadCommandSet(String classname) {
        Class<? extends ICmdWrapper> cmdset;
        try {
            cmdset = (Class<? extends ICmdWrapper>) Class.forName(classname);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        this.commands.add(cmdset);
        return true;
    }

    @Override
    public void init() {
        for (Class<? extends ICmdWrapper> cmdSet : this.commands) {
            this.init(cmdSet);
        }
    }

    private <E extends Enum<?> & ICmdWrapper> void init(Class<? extends ICmdWrapper> enumClass) {
        boolean initialized = false;
        for (ICmdWrapper item : enumClass.getEnumConstants()) {
            if (!initialized) {
                item.init(this.bot);
                initialized = true;
            }
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void onEvent(EventMessage evnt) {
        if (evnt.getMessage().startsWith(bot.getCmdPrefix())) {
            String cmd = evnt.getMessage().split(" ")[0].substring(1);
            String args = "";
            if (evnt.getMessage().indexOf(" ") > -1)
                args = evnt.getMessage().substring(evnt.getMessage().indexOf(" ") + 1);

            for (Class cmdClass : this.commands) {
                try {
                    final ICmdWrapper command = Enum.valueOf(cmdClass, cmd.toUpperCase());

                    User user = bot.getUserByNick(evnt.getSource());
                    if (user == null)
                        return;

                    if (command.requiresAdmin() && user.isLoggedIn() && bot.getAdmins().contains(user.getAccount()) || !command.requiresAdmin())
                        command.exec(bot, user, evnt.getChannel(), cmd, args);
                    else if (command.requiresAdmin() && user.isLoggedIn() && !bot.getAdmins().contains(user.getAccount()) || !user.isLoggedIn())
                        bot.io().notice(evnt.getSource(), "You don't have permission to issue that command!");

                } catch (IllegalArgumentException e) {
                    // No matching command, ignore.
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void onEvent(EventPrivMsg evnt) {
        if (evnt.getMessage().startsWith(bot.getCmdPrefix())) {
            String cmd = evnt.getMessage().split(" ")[0].substring(1);
            String args = "";
            if (evnt.getMessage().indexOf(" ") > -1)
                args = evnt.getMessage().substring(evnt.getMessage().indexOf(" ") + 1);

            for (Class cmdClass : commands) {
                try {
                    final ICmdWrapper command = Enum.valueOf(cmdClass, cmd.toUpperCase());

                    User user = bot.getUserByNick(evnt.getSource());
                    if (user == null)
                        return;

                    if (command.requiresAdmin() && user.isLoggedIn() && bot.getAdmins().contains(user.getAccount()) || !command.requiresAdmin())
                        command.execPriv(bot, bot.getUserByNick(evnt.getSource()), cmd, args);
                    else if (command.requiresAdmin() && user.isLoggedIn() && !bot.getAdmins().contains(user.getAccount()) || !user.isLoggedIn())
                        bot.io().notice(evnt.getSource(), "You don't have permission to issue that command!");

                } catch (IllegalArgumentException e) {
                    // No matching command, ignore.
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
