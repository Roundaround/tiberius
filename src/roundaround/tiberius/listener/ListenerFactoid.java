package roundaround.tiberius.listener;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import roundaround.tiberius.Core;
import roundaround.tiberius.db.jooq.Tables;
import roundaround.tiberius.db.jooq.tables.Factoids;
import roundaround.tiberius.evnt.EventMessage;
import roundaround.tiberius.evnt.EventPrivMsg;

public class ListenerFactoid extends ListenerBase {

    public ListenerFactoid(Core bot) {
        super(bot);
    }
    
    @Override
    public void init() {
        Connection conn = null;
        try {
            conn = bot.getDataObjects().getConnection();
        
            // TODO: http://www.h2database.com/html/tutorial.html#using_jooq
            
            DSLContext create = DSL.using(conn, SQLDialect.H2);
            Factoids FACTOIDS = Tables.FACTOIDS;
            
            create.execute("CREATE TABLE IF NOT EXISTS Factoids ("
                    + "id int IDENTITY PRIMARY KEY NOT NULL,"
                    + "author nvarchar(30) NOT NULL,"
                    + "created datetime NOT NULL,"
                    + "edited datetime NOT NULL,"
                    + "channel nvarchar(30) NULL DEFAULT NULL,"
                    + "name nvarchar(20) NOT NULL,"
                    + "value nvarchar(max) NOT NULL,"
                    + "alias int NULL DEFAULT NULL,"
                    + "locked bit NOT NULL DEFAULT 0,"
                    + "forgotten bit NOT NULL DEFAULT 0)");
            
            Result<Record> result = create.select().from(FACTOIDS).where(FACTOIDS.NAME.equal("test")).fetch();
            if (result.isEmpty()) {
                create.insertInto(FACTOIDS, FACTOIDS.AUTHOR, FACTOIDS.CREATED, FACTOIDS.EDITED, FACTOIDS.NAME, FACTOIDS.VALUE)
                    .values("Roundaround", new Timestamp(new Date().getTime()), new Timestamp(new Date().getTime()), "test", "This is a test factoid!");
                result = create.select().from(FACTOIDS).where(FACTOIDS.NAME.equal("test")).fetch();
            }
            
            for (Record r : result)
                System.out.println(r.getValue(Tables.FACTOIDS.NAME) + ": " + r.getValue(Tables.FACTOIDS.VALUE));
            
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ignore) {
                }
            }
        }
    }
    
    @Override
    public void onEvent(EventMessage evnt) {
        
    }
    
    @Override
    public void onEvent(EventPrivMsg evnt) {
        
    }

}
