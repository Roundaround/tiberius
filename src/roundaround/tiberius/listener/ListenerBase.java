package roundaround.tiberius.listener;

import roundaround.tiberius.Core;
import roundaround.tiberius.evnt.EventEndOfNames;
import roundaround.tiberius.evnt.EventJoin;
import roundaround.tiberius.evnt.EventKick;
import roundaround.tiberius.evnt.EventMessage;
import roundaround.tiberius.evnt.EventNames;
import roundaround.tiberius.evnt.EventNick;
import roundaround.tiberius.evnt.EventNotice;
import roundaround.tiberius.evnt.EventPart;
import roundaround.tiberius.evnt.EventPing;
import roundaround.tiberius.evnt.EventPrivMsg;
import roundaround.tiberius.evnt.EventQuit;
import roundaround.tiberius.evnt.EventSelfJoin;
import roundaround.tiberius.evnt.EventSelfKick;
import roundaround.tiberius.evnt.EventSelfPart;
import roundaround.tiberius.evnt.EventWho;
import roundaround.tiberius.evnt.EventWhoIs;

public abstract class ListenerBase {
	protected Core bot;
	
	protected ListenerBase(Core bot) {
		this.bot = bot;
	}
	
	public void init() {}

	public void onEvent(EventEndOfNames evnt) {}
	public void onEvent(EventJoin evnt) {}
    public void onEvent(EventKick evnt) {}
	public void onEvent(EventMessage evnt) {}
	public void onEvent(EventNames evnt) {}
	public void onEvent(EventNick evnt) {}
	public void onEvent(EventNotice evnt) {}
	public void onEvent(EventPart evnt) {}
	public void onEvent(EventPing evnt) {}
	public void onEvent(EventPrivMsg evnt) {}
	public void onEvent(EventQuit evnt) {}
	public void onEvent(EventSelfJoin evnt) {}
    public void onEvent(EventSelfKick evnt) {}
    public void onEvent(EventSelfPart evnt) {}
	public void onEvent(EventWho evnt) {}
	public void onEvent(EventWhoIs evnt) {}
}
