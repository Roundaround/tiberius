package roundaround.tiberius.listener;

import java.io.IOException;

import roundaround.tiberius.Core;
import roundaround.tiberius.Utils.Pair;
import roundaround.tiberius.Utils.User;
import roundaround.tiberius.evnt.EventEndOfNames;
import roundaround.tiberius.evnt.EventJoin;
import roundaround.tiberius.evnt.EventKick;
import roundaround.tiberius.evnt.EventNames;
import roundaround.tiberius.evnt.EventNick;
import roundaround.tiberius.evnt.EventPart;
import roundaround.tiberius.evnt.EventQuit;
import roundaround.tiberius.evnt.EventSelfKick;
import roundaround.tiberius.evnt.EventSelfPart;
import roundaround.tiberius.evnt.EventWho;

public class ListenerUser extends ListenerBase {
	public ListenerUser(Core bot) {
		super(bot);
	}
	
	@Override
	public void onEvent(EventEndOfNames evnt) {
		for (Pair<String, String> mapping : bot.getUserChanMap()) {
			if (mapping.getRight().equalsIgnoreCase(evnt.getChannel())) {
				try {
					bot.io().who(mapping.getLeft());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		bot.getUserChanMap().add(new Pair<String, String>("*", evnt.getChannel().toLowerCase()));
	}
	
	@Override
	public void onEvent(EventNick evnt) {
		User user = bot.getUserByNick(evnt.getNickOld());
		bot.getTrackedUsers().remove(user);
		user.setNick(evnt.getNickNew());
		bot.getTrackedUsers().add(user);
		
		for (Pair<String, String> mapping : bot.getUserChanMap()) {
			if (mapping.getLeft().equals(evnt.getNickOld())) {
				bot.getUserChanMap().remove(mapping);
				bot.getUserChanMap().add(new Pair<String, String>(evnt.getNickNew(), mapping.getRight()));
			}
		}
	}
	
	@Override
	public void onEvent(EventNames evnt) {
		if (bot.areChanUsersMapped(evnt.getChannel())) {
			for (Pair<String, String> mapping : bot.getUserChanMap()) {
				if (mapping.getRight().equalsIgnoreCase(evnt.getChannel()))
					bot.getUserChanMap().remove(mapping);
			}
		}
		
		for (String nick : evnt.getNicks()) {
			bot.getUserChanMap().add(new Pair<String, String>(nick, evnt.getChannel()));
		}
	}
	
	@Override
	public void onEvent(EventJoin evnt) {
		bot.getTrackedUsers().remove(evnt.getUser());
		bot.getTrackedUsers().add(evnt.getUser());
		
		bot.getUserChanMap().add(new Pair<String, String>(evnt.getUser().getNick(), evnt.getChannel()));
	}
	
	@Override
	public void onEvent(EventPart evnt) {
		boolean stillListening = false;
		
		for (Pair<String, String> mapping : bot.getUserChanMap()) {
			if (mapping.getRight().equalsIgnoreCase(evnt.getChannel()) && mapping.getLeft().equals(evnt.getNick()))
				bot.getUserChanMap().remove(mapping);
			else if (mapping.getRight().equalsIgnoreCase(evnt.getChannel()))
				stillListening = true;
		}
		
		if (!stillListening) {
			User user = bot.getUserByNick(evnt.getNick());
			bot.getTrackedUsers().remove(user);
		}
	}
    
    @Override
    public void onEvent(EventKick evnt) {
        boolean stillListening = false;
        
        for (Pair<String, String> mapping : bot.getUserChanMap()) {
            if (mapping.getRight().equalsIgnoreCase(evnt.getChannel()) && mapping.getLeft().equals(evnt.getNick()))
                bot.getUserChanMap().remove(mapping);
            else if (mapping.getRight().equalsIgnoreCase(evnt.getChannel()))
                stillListening = true;
        }
        
        if (!stillListening) {
            User user = bot.getUserByNick(evnt.getNick());
            bot.getTrackedUsers().remove(user);
        }
    }

	@Override
	public void onEvent(EventQuit evnt) {
		bot.getTrackedUsers().remove(bot.getUserByNick(evnt.getNick()));
		
		for (Pair<String, String> mapping : bot.getUserChanMap()) {
			if (mapping.getLeft().equals(evnt.getNick()))
				bot.getUserChanMap().remove(mapping);
		}
	}

	@Override
	public void onEvent(EventWho evnt) {
		bot.getTrackedUsers().remove(evnt.getUser());
		bot.getTrackedUsers().add(evnt.getUser());
	}
	
	@Override
    public void onEvent(EventSelfKick evnt) {
	    // TODO: Remove channel from tracking system, and all users who are unique to it.
	}
	
	@Override
    public void onEvent(EventSelfPart evnt) {
        // TODO: Remove channel from tracking system, and all users who are unique to it.
	}
}
