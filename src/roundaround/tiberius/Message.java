package roundaround.tiberius;

import java.util.StringTokenizer;
import java.util.Vector;

public class Message {
    protected String raw = null;
    protected String prefix = null;
    protected String command = null;
    protected String arguments[] = null;
    protected String trailing = null;
    
    private Message(String raw, String prefix, String command, String arguments[], String trailing) {
    	this.raw = raw;
    	this.prefix = prefix;
    	this.command = command;
    	this.arguments = arguments;
    	this.trailing = trailing;
    }

    public static Message parse(String raw) {
    	if (raw == null)
    		return null;
    	
        String prefix = null;
        String command = null;
        String arguments[] = null;
        String trailing = null;
        StringTokenizer st = new StringTokenizer(raw, " ");
        String token = null;

		if (st.hasMoreTokens()) {
			token = st.nextToken();
		} else {
			throw new IllegalArgumentException("Bad message syntax: " + raw);
		}

		if (token.charAt(0) == ':') {
			// parse prefix & command
			prefix = token.substring(1);

			if (st.hasMoreTokens()) {
				command = st.nextToken();
			} else {
				throw new IllegalArgumentException("Bad message syntax: " + raw);
			}
		} else {
			// message has no prefix
			command = token;
		}

		if ((command == null) || command.equals("")) {
			throw new IllegalArgumentException(
					"Command cannot be null or empty! " + "command = "
							+ command);
		}

		// parse parameters
		Vector<String> v = new Vector<String>(5, 5);
		while (st.hasMoreTokens()) {
			token = st.nextToken();

			// check for trailing part
			if (token.charAt(0) == ':') {
				trailing = token.substring(1);

				// parse whole trailing part and we're through
				while (st.hasMoreTokens()) {
					trailing += " " + st.nextToken();
				}
			} else {
				v.add(token);
			}
		}

		// create arguments -array
		int size = v.size();
		if (size > 0) {
			arguments = new String[size];

			for (int i = 0; i < size; i++) {
				arguments[i] = (String) v.elementAt(i);
			}
		}
		
		return new Message(raw, prefix, command, arguments, trailing);
    }
}
