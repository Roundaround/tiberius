package roundaround.tiberius.exception;

public class MissingConfigurationAttributeException extends Exception {

    private static final long serialVersionUID = -8206751838064635362L;

    public MissingConfigurationAttributeException(String field, String attribute) {
        super("The required attribute '" + attribute + "' of field '" + field + "' is missing from this bot's configuration file.");
    }
}
