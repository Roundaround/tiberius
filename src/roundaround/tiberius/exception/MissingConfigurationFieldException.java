package roundaround.tiberius.exception;

public class MissingConfigurationFieldException extends Exception {
    
    private static final long serialVersionUID = -8365228495280215991L;

    public MissingConfigurationFieldException(String field) {
        super("The required field '" + field + "' is missing from this bot's configuration file.");
    }
}
