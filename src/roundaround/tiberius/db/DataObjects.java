package roundaround.tiberius.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataObjects {
    private final String url;
    
    public DataObjects(String url) {
        this.url = url;
    }
    
    public void initDatabase() {
        Connection conn = null;
        try {
            conn = getConnection();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ignore) {
                }
            }
        }
    }
    
    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("org.h2.Driver");
        return DriverManager.getConnection(this.url);
    }
}